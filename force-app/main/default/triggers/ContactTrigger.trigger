trigger ContactTrigger on Contact (after delete, after update, after insert) 
{
    if(Trigger.isAfter && Trigger.isInsert)
    {
        ContactTriggerHandler.onAfterInsert(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        ContactTriggerHandler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.isDelete)
    {
        ContactTriggerHandler.onAfterDelete(Trigger.oldMap);   //
    }

}
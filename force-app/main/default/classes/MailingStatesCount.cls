public  class MailingStatesCount implements Database.Batchable<sObject> 
{
    public Database.QueryLocator start(Database.BatchableContext bc)
    { 
        
        return Database.getQueryLocator(' SELECT Name, Mailing_State__c FROM Account WHERE Mailing_State__c != NULL'); 
    }
    public void execute(Database.BatchableContext bc, List<Account> lstAccounts)
    {
        System.debug(lstAccounts);
        Map<String, Integer> mapOfMsCount = new Map<String, Integer>();
        for(Account objAcc : lstAccounts )
        {
            if(!mapOfMsCount.containsKey(objAcc.Mailing_State__c) )
            {
                mapOfMsCount.put(objAcc.Mailing_State__c,1);
            }
            else 
            {
                mapOfMsCount.put(objAcc.Mailing_State__c,mapOfMsCount.get(objAcc.Mailing_State__c)+1);
            }
        }
        List<MailingState__c> lstMailState = [SELECT Id, Name,No_Of_Accounts__c FROM MailingState__c ];
        List<MailingState__c> lstDelete = new List<MailingState__c>();
        List<MailingState__c> upsertlst = new List<MailingState__c>();
        Set<String> setExistingState = new Set<String>();
        for(MailingState__c objMs : lstMailState)
        {
            if(objMs.No_Of_Accounts__c != mapOfMsCount.get(objMs.Name))
            {
                objMs.No_Of_Accounts__c = mapOfMsCount.get(objMs.Name);
                upsertlst.add(objMs);
            }
            setExistingState.add(objMs.Name);
            if(objMs.No_Of_Accounts__c == NULL)
            {
                lstDelete.add(objMs);
            }
        }
        Delete lstDelete;
        for(String strMs : mapOfMsCount.keySet())
        {
            if(!setExistingState.contains(strMs))
            {
                MailingState__c  objMS = new MailingState__c(Name = strMs, No_Of_Accounts__c = mapOfMsCount.get(strMs));
                upsertlst.add(objMS);
            }
        }
        if(!upsertlst.isEmpty())
        {
            upsert upsertlst; 
        }
    }
    public void finish(Database.BatchableContext bc)
    {
        System.debug('Done');
    }
}
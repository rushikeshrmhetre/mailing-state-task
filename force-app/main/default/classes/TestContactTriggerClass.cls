@isTest
public class TestContactTriggerClass 
{
    @testSetup 
    public static void setupAccount()
    {
        List<Account> lstAccount = new List<Account>();
        Account objAcc = new Account();
        objAcc.Name ='TestclassAccount';
        lstAccount.add(objAcc); 
        
        Account objAccSecond = new Account();
        objAccSecond.Name ='TestclassAccount2';
        lstAccount.add(objAccSecond);
        
        Test.startTest();
        insert lstAccount;
        System.assertEquals('TestclassAccount', objAcc.Name);
        System.assertEquals('TestclassAccount2', objAccSecond.Name);
        Test.stopTest();  
    }
    
    @isTest    
    public static void testCreationOfContacts() 
    {   
        Id accID = [SELECT ID FROM Account WHERE Name='TestclassAccount' LIMIT 1].Id;
        List<Contact> lstContacts = new List<Contact>();
        for(Integer count = 1; count <=10; count++)
        {
            Contact objCon = new Contact();
            objCon.LastName = 'TestCon1'+ count;
            objCon.MailingState = 'DL';
            objCon.AccountId = accID;
            lstContacts.add(objCon);    
        }
        
        for(Integer count = 1; count <= 10; count++)
        {
            Contact objCon = new Contact();
            objCon.LastName = 'TestCon2'+ count;
            objCon.MailingState = 'MH';
            objCon.AccountId = accID;
            lstContacts.add(objCon);    
        }
        
        Test.startTest();
        insert lstContacts;
        Account objAcc = [SELECT All_Mailing_States__c FROM Account WHERE Id =: accID];
        System.assertEquals('DL, MH',objAcc.All_Mailing_States__c);
        Test.stopTest();
  
    }
   
    @isTest
    public static void testDeleteContact()
    {	
        Id accID = [SELECT ID FROM Account WHERE Name = 'TestclassAccount' LIMIT 1].Id;
        
        Contact objCon = new Contact( LastName = 'TestConUpdate', MailingState = 'KL', AccountId = accID );
        insert objCon;
        
        Test.startTest();
        Account objAcc = [SELECT All_Mailing_States__c FROM Account WHERE Id =: accID];
        System.assertEquals('KL', objAcc.All_Mailing_States__c);
        delete objCon;   
        objAcc = [SELECT All_Mailing_States__c FROM Account WHERE Id =: accID];
        System.assertEquals(NULL, objAcc.All_Mailing_States__c);

        Test.stopTest();
    }
    
    @istest
    public static void testUpdateContact()
    {
 		Id accID = [SELECT ID FROM Account WHERE Name = 'TestclassAccount' LIMIT 1].Id;
        Id accID2 = [SELECT ID FROM Account WHERE Name = 'TestClassAccount2' LIMIT 1].Id;
        List<contact> lstConUpdate = new List<Contact>();
        Contact objCon = new Contact( LastName = 'TestConUpdate', MailingState='KL', AccountId = accID );
        insert objCon;
        objCon.MailingState = 'UP';
        objCon.AccountId = accID2;
        lstConUpdate.add(objCon);
        
        Contact objCon2 = new Contact( LastName = 'TestConUpdate2', MailingState='NY', AccountId = accID );
        insert objCon2;
        objCon2.MailingState = 'MH';
        lstConUpdate.add(objCon2);
        
        Test.startTest();
        update lstConUpdate;
        Account objAcc = [SELECT All_Mailing_States__c FROM Account WHERE Id =: accID];
        Account objAcc1 = [SELECT All_Mailing_States__c FROM Account WHERE Id =: accID2];
        System.assertEquals('MH',objAcc.All_Mailing_States__c);
        System.assertEquals('UP',objAcc1.All_Mailing_States__c);
        Test.stopTest();
    }
}
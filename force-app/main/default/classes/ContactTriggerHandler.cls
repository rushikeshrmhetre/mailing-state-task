public  class ContactTriggerHandler 
{
    public static void onAfterInsert(List<Contact> lstNewContacts )
    {
        Set<ID> setAccountIds = new Set<Id>();    // set to store all unique account Ids.
        for(Contact objCon : lstNewContacts )       // filter contacts with Account Ids and mailing State 
        {
            if(objCon.AccountId != NULL && objCon.MailingState != NULL)
            {
                setAccountIds.add(objCon.AccountId);             // add the Account Id to the set
            }
        }
        ContactTriggerHandler.updateAccountWithMailingStates(setAccountIds); // pass the set of accounts to bussiness function
    }

    public static void onAfterUpdate(Map<Id, Contact> mapOldContacts, Map<Id, Contact> mapNewContacts)
    {
        Set<ID> setAccountIds = new Set<Id>();               
        for(Id conId : mapNewContacts.keySet())             // iteration map Keyset i.e. ContactIds
        {
            if(( mapNewContactS.get(conId).AccountId != mapOldContacts.get(conId).AccountId || 
                mapNewContactS.get(conId).MailingState != mapOldContacts.get(conId).MailingState) &&
                mapNewContacts.get(conId).AccountId != NULL && mapNewContacts.get(conId).MailingState != NULL)  // filtering map as per requirement
            {
                if(mapNewContacts.get(conId).AccountId != mapOldContacts.get(conId).AccountId)
                {
                    setAccountIds.add(mapOldContacts.get(conId).AccountId);                         // adding account account id into set Account.
                }
                setAccountIds.add(mapNewContacts.get(conId).AccountId);                  
            }
        }
        ContactTriggerHandler.updateAccountWithMailingStates(setAccountIds);
    }

    public static void onAfterDelete(Map<Id, Contact> mapOldContacts) 
    {
        Set<ID> setAccountIds = new Set<Id>();
        for(Id conId : mapOldContacts.keySet())                   // iterating on  contact ids
        {
            if(mapOldContacts.get(conId).AccountId != NULL && mapOldContacts.get(conId).MailingState != NULL) // filtering ids with requirment
            {
                setAccountIds.add(mapOldContacts.get(conId).AccountId);         // add Account IDs to set.
            }
        }
        ContactTriggerHandler.updateAccountWithMailingStates(setAccountIds);
    }

    //Business Functions
    public static void updateAccountWithMailingStates(Set<Id> setAccountIds)
    {
        Map<Id, String> mapAccID_MailingState = new Map<Id, String>();      //  used map to store and account Ids and States
		
        for( Contact objCon : [SELECT AccountId, MailingState FROM Contact WHERE AccountID IN : setAccountIds])   
        {   
			           
            if(!mapAccID_MailingState.containsKey(objCon.AccountId))             //check AccountId is present in map.     
            {	
              
               		mapAccID_MailingState.put(objCon.AccountId, objCon.MailingState); 
              																		// if present then add value.      
            }
            else                                                       // else insert new key with Account Id and state values.
            {   
                Boolean boolCheckState = mapAccID_MailingState.get(objCon.AccountId).Contains(objCon.MailingState); // varifying values is already present or not.
                if(!boolCheckState)
                {
                    String strCSVMailingStates = mapAccID_MailingState.get(objCon.AccountId) + ', ' + objCon.MailingState; // if not present appending states 
                    mapAccID_MailingState.put(objCon.AccountId, strCSVMailingStates);                                   // updating values in map
                }
              
            }
        }
        
        for(Account objAcc : [SELECT Id, (SELECT Id FROM Contacts) FROM Account WHERE Id IN : setAccountIds])
        {
            if(objAcc.Contacts.size() == 0)   
            {
                mapAccID_MailingState.put(objAcc.Id, NULL);  
            }
        }
        
        List<Account> lstAccounts = new List<Account>();
        for(ID accId : mapAccID_MailingState.keySet())       // iterating on map key values.
        {
            Account objAcc = new Account();                  // created account object to adding map values.
            objAcc.ID = accId;                                // assigning key as id.
            objAcc.All_Mailing_States__c = mapAccID_MailingState.get(accId);  // assigning values to account field All mailing state from map
            lstAccounts.add(objAcc);                              // add the object into list.
        }

        if(!lstAccounts.isEmpty())
        {
            update lstAccounts;                        // check whether list is empty or not, if not empty update list.
        }
    }
}